﻿-- Mehanik 3 Core, update world db.


-- ## Transmogrification
INSERT INTO `creature_template` (`entry`, `modelid1`, `modelid2`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES
(190010, 19646, 0, "Warpweaver", "Transmogrifier", NULL, 0, 80, 80, 2, 35, 35, 1, 1, 0, 500, 500, 0, 350, 1, 2000, 0, 1, 0, 0, 0, 0, 7, 138936390, 0, 0, 0, '', 0, 3, 1, 1, 1, 1, 0, 0, 1, 0, 0, 'Creature_Transmogrify', 0);

SET @TEXT_ID := 50000;
INSERT INTO `npc_text` (`ID`, `text0_0`, `WDBVerified`) VALUES
(@TEXT_ID, 'Transmogrification allows you to change how your items look like without changing the stats of the items.\r\nItems used in transmogrification are no longer refundable, tradeable and are bound to you.\r\nUpdating a menu updates the view and prices.\r\n\r\nNot everything can be transmogrified with eachother.\r\nRestrictions include but are not limited to:\r\nOnly armor and weapons can be transmogrified\r\nGuns, bows and crossbows can be transmogrified with eachother\r\nFishing poles can not be transmogrified\r\nYou must be able to equip both items used in the process.\r\n\r\nTransmogrifications stay on your items as long as you own them.\r\nIf you try to put the item in guild bank or mail it to someone else, the transmogrification is stripped.\r\n\r\nYou can also remove transmogrifications for free at the transmogrifier.', 1),
(@TEXT_ID+1, 'You can save your own transmogrification sets.\r\n\r\nTo save, first you must transmogrify your equipped items.\r\nThen when you go to the set management menu and go to save set menu,\r\nall items you have transmogrified are displayed so you see what you are saving.\r\nIf you think the set is fine, you can click to save the set and name it as you wish.\r\n\r\nTo use a set you can click the saved set in the set management menu and then select use set.\r\nIf the set has a transmogrification for an item that is already transmogrified, the old transmogrification is lost.\r\nNote that same transmogrification restrictions apply when trying to use a set as in normal transmogrification.\r\n\r\nTo delete a set you can go to the set\'s menu and select delete set.', 1);

SET @STRING_ENTRY := 11100;
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES
(@STRING_ENTRY+0, 'Item transmogrified'),
(@STRING_ENTRY+1, 'Equipment slot is empty'),
(@STRING_ENTRY+2, 'Invalid source item selected'),
(@STRING_ENTRY+3, 'Source item does not exist'),
(@STRING_ENTRY+4, 'Destination item does not exist'),
(@STRING_ENTRY+5, 'Selected items are invalid'),
(@STRING_ENTRY+6, 'Not enough money'),
(@STRING_ENTRY+7, 'You don\'t have enough tokens'),
(@STRING_ENTRY+8, 'Transmogrifications removed'),
(@STRING_ENTRY+9, 'There are no transmogrifications'),
(@STRING_ENTRY+10, 'Invalid name inserted');


-- ## Npc С++ Teleports
DELETE FROM creature_template WHERE entry = '100000';
INSERT INTO creature_template (entry, modelid1, name, subname, IconName, gossip_menu_id, minlevel, maxlevel, Health_mod, Mana_mod, Armor_mod, faction_A, faction_H, npcflag, speed_walk, speed_run, scale, rank, dmg_multiplier, unit_class, unit_flags, TYPE, type_flags, InhabitType, RegenHealth, flags_extra, ScriptName) VALUES
('100000', '21572', "Мастер телепортации", "", 'Directions', '50000', 71, 71, 1.56, 1.56, 1.56, 35, 35, 3, 1, 1.14286, 1.25, 1, 1, 1, 2, 7, 138936390, 3, 1, 2, 'TeLe_gossip_codebox');

DELETE FROM creature_template_addon WHERE Entry = '100000' ;
INSERT INTO creature_template_addon (entry, mount, bytes1, bytes2, emote, path_id, auras) VALUES
('100000', 0, 0, 0, 0, 0, '35766');

DELETE FROM creature WHERE ID = '100000';
ALTER TABLE creature AUTO_INCREMENT=200000;
INSERT INTO creature (id, map, spawnMask, phaseMask, modelid, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, curhealth, curmana) VALUES
-- standart
('100000', 0, 1, 1, 0, -13180.5, 342.503, 43.1936, 4.32977, 25, 0, 13700, 6540),
('100000', 530, 1, 1, 0, -3862.69, -11645.8, -137.629, 2.38273, 25, 0, 13700, 6540),
('100000', 0, 1, 1, 0, -4898.37, -965.118, 501.447, 2.25986, 25, 0, 13700, 6540),
('100000', 0, 1, 1, 0, -8845.09, 624.828, 94.2999, 0.44062, 25, 0, 13700, 6540),
('100000', 1, 1, 1, 0, 1599.25, -4375.85, 10.0872, 5.23641, 25, 0, 13700, 6540),
('100000', 1, 1, 1, 0, -1277.65, 72.9751, 128.742, 5.95567, 25, 0, 13700, 6540),
('100000', 0, 1, 1, 0, 1637.21, 240.132, -43.1034, 3.13147, 25, 0, 13700, 6540),
('100000', 530, 1, 1, 0, 9741.67, -7454.19, 13.5572, 3.14231, 25, 0, 13700, 6540),
('100000', 571, 1, 1, 0, 5807.06, 506.244, 657.576, 5.54461, 25, 0, 13700, 6540),
('100000', 1, 1, 1, 0, 9866.83, 2494.66, 1315.88, 5.9462, 25, 0, 13700, 6540),
('100000', 0, 1, 1, 0, -14279.8, 555.014, 8.90011, 3.97606, 25, 0, 13700, 6540),
('100000', 530, 1, 1, 0, -1888.65, 5355.88, -12.4279, 1.25883, 25, 0, 13700, 6540),
-- start
('100000', '1', '1', '1', '0', '1488.45', '-4425.77', '24.8373', '1.49851', '300', '0', '116160', '0'),
('100000', '530', '1', '1', '0', '-3965.07', '-13919.4', '100.417', '4.78463', '300', '0', '116160', '0'),
('100000', '1', '1', '1', '0', '10326', '821.839', '1326.42', '2.62777', '300', '0', '116160', '0'),
('100000', '0', '1', '1', '0', '-6235.08', '331.416', '382.952', '3.15366', '300', '0', '116160', '0'),
('100000', '530', '1', '1', '0', '10352.2', '-6362.21', '34.5248', '2.40537', '300', '0', '116160', '0'),
('100000', '0', '1', '1', '0', '-8879.55', '569.219', '93.2032', '2.23032', '300', '0', '116160', '0'),
('100000', '1', '1', '1', '0', '-2907.19', '-258.765', '52.9415', '3.88903', '300', '0', '116160', '0'),
('100000', '0', '1', '1', '0', '1657.35', '1673.21', '120.719', '0.125226', '300', '0', '116160', '0'),
('100000', '1', '1', '1', '0', '-605.27', '-4247.45', '38.956', '2.96802', '300', '0', '116160', '0'),
('100000', '0', '1', '1', '0', '-8946.58', '-133.578', '83.7171', '2.77288', '300', '0', '116160', '0');

DELETE FROM gameobject WHERE ID=194394 AND guid>199999;
ALTER TABLE gameobject AUTO_INCREMENT=200000;
INSERT INTO gameobject (id, map, spawnMask, phaseMask, position_x, position_y, position_z, orientation, rotation2, rotation3, spawntimesecs, state) VALUES
(194394, 1, 1, 1, 1601.08, -4378.69, 9.9846, 2.14362, 0.878068, 0.478536, 25, 1),
(194394, 0, 1, 1, -14281.9, 552.564, 8.90382, 0.860144, 0.416936, 0.908936, 25, 1),
(194394, 0, 1, 1, -8842.09, 626.358, 94.0868, 3.61363, 0.972276, -0.233836, 25, 1),
(194394, 0, 1, 1, -4900.47, -962.585, 501.455, 5.40538, 0.424947, -0.905218, 25, 1),
(194394, 1, 1, 1, 9869.91, 2493.58, 1315.88, 5.9462, 0.167696, -0.985839, 25, 1),
(194394, 530, 1, 1, -3864.92, -11643.7, -137.644, 2.38273, 0.928875, 0.370392, 25, 1),
(194394, 530, 1, 1, -1887.62, 5359.09, -12.4279, 4.40435, 0.758205, 0.652017, 25, 1),
(194394, 571, 1, 1, 5809.55, 503.975, 657.526, 5.54461, 0.360952, -0.932584, 25, 1),
(194394, 530, 1, 1, 9738.28, -7454.19, 13.5605, 3.14231, 1, -0.000358625, 25, 1),
(194394, 0, 1, 1, 1633.75, 240.167, -43.1034, 3.13147, 0.999987, 0.00506132, 25, 1),
(194394, 0, 1, 1, -13181.8, 339.356, 42.9805, 1.18013, 0.556415, 0.830904, 25, 1),
(194394, 1, 1, 1, -1274.45, 71.8601, 128.159, 2.80623, 0.985974, 0.166898, 25, 1);


-- ## Npc Professions
DELETE FROM `creature_template` WHERE `entry` = 500000;
INSERT INTO `creature_template` (`entry`, `modelid1`, `modelid2`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES
(500000, 22847, 0, "Professions", "", NULL, 0, 80, 80, 2, 35, 35, 1, 1, 0, 500, 500, 0, 350, 1, 2000, 0, 1, 0, 0, 0, 0, 7, 138936390, 0, 0, 0, '', 0, 3, 1, 1, 1, 1, 0, 0, 1, 0, 0, 'Professions_NPC', 0);


-- ## AutoBanned Passive Anticheat System!
REPLACE INTO `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`) VALUES
(12000, '|cFFFFFC00[ANTICHEAT]|cFF00FFFF[|cFF60FF00%s|cFF00FFFF] Banned for cheating!|r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '|cFFFFFC00[Античит]|cFF00FFFF[|cFF60FF00%s|cFF00FFFF] Забанен за использование читов!|r');


-- ## Fake Online System!
REPLACE INTO `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`) VALUES
(12001, 'Do not Disturb, аway from keyboard.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Не беспокоить, отошёл.');


-- ## Mod Weekend 50% XP Bonus
REPLACE INTO `game_event` (`eventEntry`, `start_time`, `end_time`, `occurence`, `length`, `holiday`, `description`, `world_event`) VALUES 
('85', '2012-02-11 00:00:00', '2020-02-11 00:00:00', '10080', '2880', '0', 'Weekend 50% XP Bonus', '0');


-- ## Переводы.
UPDATE `trinity_string` SET `content_loc8`='|cFFFFFC00[Anticheat_Mehanik]|cFF00FFFF[|cFF60FF00%s|cFF00FFFF] Забанен за Cheat!|r' WHERE (`entry`='66666');
UPDATE `trinity_string` SET `content_loc8`='|cffffff00[|c00077766Автоматическое сообщение|cffffff00]: |cFFF222FF%s|r' WHERE (`entry`='11000');
UPDATE `trinity_string` SET `content_loc8`='|cffffff00[|c1f40af20Аннонс от|r |cffff0000%s|cffffff00]:|r %s|r' WHERE (`entry`='787');
UPDATE `trinity_string` SET `content_loc8`='|cffff0000[Арена-Аннонс]:|r %s -- Joined : %ux%u : %u|r' WHERE (`entry`='718');
UPDATE `trinity_string` SET `content_loc8`='|cffff0000[Арена-Аннонс]:|r %s -- Exited : %ux%u : %u|r' WHERE (`entry`='719');
UPDATE `trinity_string` SET `content_loc8`='|cffff0000[Бг-Аннонс]:|r %s -- [%u-%u] A: %u/%u, H: %u/%u|r' WHERE (`entry`='712');
UPDATE `trinity_string` SET `content_loc8`='|cffff0000[Бг-Аннонс]:|r %s -- [%u-%u] Started!|r' WHERE (`entry`='717');
UPDATE `trinity_string` SET `content_loc8`='mehanik: %s дал мут %s на %u минут, по причине: %s' WHERE (`entry`='11003');
UPDATE `trinity_string` SET `content_loc8`='mehanik: %s дал бан %s на %s, по причине: %s' WHERE (`entry`='11006');
UPDATE `trinity_string` SET `content_loc8`='mehanik: %s дал бан %s permanetly, по причине: %s' WHERE (`entry`='11007');
UPDATE `trinity_string` SET `content_loc8`='mehanik: %s дал бан персонажу %s на %s, по причине: %s' WHERE (`entry`='11004');
UPDATE `trinity_string` SET `content_loc8`='\r\nmehanik: %s дал бан персонажу %s permanetly, по причине: %s' WHERE (`entry`='11005');
UPDATE `trinity_string` SET `content_loc8`='[mehanik] %s' WHERE (`entry`='3');
UPDATE `trinity_string` SET `content_loc8`='Время работы: %s' WHERE (`entry`='13');
UPDATE `trinity_string` SET `content_loc8`='Онлайн игроков: %u (max: %u)' WHERE (`entry`='60');
UPDATE `trinity_string` SET `content_loc8`='|Hplayer:$N|h[$N]|h получил достижение $a!' WHERE (`entry`='810');

-- ## Disables
DELETE FROM `disables` WHERE `sourceType`=0 AND `entry` IN (23789,61904,40851,61905,39090);
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `comment`) VALUES
(0, 23789, 1, 'Stoneclaw Totem TEST - can crash client by spawning too many totems'),
(0, 61904, 1, 'Magma Totem TEST - can crash client by spawning too many totems'),
(0, 40851, 1, 'Disable spell which players can use with Mind Control'),
(0, 61905, 1, 'spellcrash2'),
(0, 39090, 1, 'Positive Charge'); -- http://www.wowhead.com/spell=39090
INSERT IGNORE INTO `disables` VALUES (7, 650, 0, '', '', 'Mmaps - Trial of the Champion');
INSERT IGNORE INTO `disables` VALUES (7, 649, 0, '', '', 'Mmaps - Trial of the Crusader');


-- ## Warden
DELETE FROM `warden_checks` WHERE `id` IN (791,788,789,790,792,793,794,794);
INSERT INTO `warden_checks` (`id`, `type`, `data`, `str`, `address`, `length`, `result`, `comment`) values
('791','243','','','7728616','1','75','Simplefly'),
('788','243','','','9993834','1','74','Simplefly'),
('789','243','','','9993835','1','52','Simplefly'),
('790','243','','','7277234','8','85F60F86F2000000','Simplefly - speedhack'),
('792','191','1E15223BF7DD461696FB25488B72FDE017D1AB7056A1273B','','583955','40','','Hitch Hiker - 0x05 Packet'),
('793','191','2FD8E5D46A57F471DE8C7DA01815AF6653B4B34BAEFD3F6B','','583955','40','','Hitch Hiker - 20 HMAC Seed'),
('794','191','98FD2423C3EC3B0867A960C94FF31583428A72C3AEFD3F6B','','583955','40','','Hitch Hiker - 16 HMAC Seed'),
('795','191','36F330EF890C237526929B959613982D3D035DABAEFD3F6B','','583955','40','','Hitch Hiker - No HMAC Seed');


-- ## Item: Solace of the Defeated (Normal), http://old.wowhead.com/item=47041
-- ## Item: Solace of the Defeated (Heroic), http://old.wowhead.com/item=47059
DELETE FROM `spell_proc_event` WHERE `entry` IN (67698, 67752);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES
(67698, 0, 0, 0, 0, 0, 0, 65536, 0, 0, 0),
(67752, 0, 0, 0, 0, 0, 0, 65536, 0, 0, 0);


-- ## Item: Trauma, http://www.wowhead.com/item=50028
-- ## Item: Trauma (H), http://www.wowhead.com/item=50685
DELETE FROM `spell_proc_event` WHERE `entry` IN (71865, 71868);
INSERT INTO `spell_proc_event`(`entry`,`SchoolMask`,`SpellFamilyName`,`SpellFamilyMask0`,`SpellFamilyMask1`,`SpellFamilyMask2`,`procFlags`,`procEx`,`ppmRate`,`CustomChance`,`Cooldown`) VALUES
(71865, 0, 0, 4294967295, 4294967295, 4294967295, 0, 131072, 0, 6, 0),
(71868, 0, 0, 4294967295, 4294967295, 4294967295, 0, 131072, 0, 6, 0);


-- ## Elemental focus(shaman talent) correct proc.
DELETE FROM spell_proc_event WHERE entry = 16164;
INSERT INTO spell_proc_event (entry, SchoolMask, SpellFamilyName, SpellFamilyMask0, SpellFamilyMask1, SpellFamilyMask2, procFlags, procEx, ppmRate, CustomChance, Cooldown) VALUES 
(16164, 0, 11, 2416967683, 4096, 0, 0, 2, 0, 0, 0);


-- ## Fix Dancing Rune Weapon.
REPLACE INTO `spell_proc_event` (entry, procFlags) VALUES
('49028','69652');


-- ## Fix Lifebloom.
DELETE FROM spell_bonus_data WHERE entry IN (33778, 33763);
INSERT INTO spell_bonus_data (entry, direct_bonus, dot_bonus, ap_bonus, ap_dot_bonus, comments) VALUES
(33778, 0, 0, 0, 0, 'Druid - Lifebloom final heal'),
(33763, 0.516, 0.0952, 0, 0, 'Druid - Lifebloom HoT(rank 1)');


-- ## Sap removes Stealth.
INSERT INTO spell_linked_spell (spell_trigger, spell_effect, type, comment) VALUES 
(6770, -1784, 1, 'Sap(rank 1) removes Stealth'),
(2070, -1784, 1, 'Sap(rank 2) removes Stealth'),
(11297, -1784, 1, 'Sap(rank 3) removes Stealth'),
(51724, -1784, 1, 'Sap(rank 4) removes Stealth');


-- ## Priest: Shadowfiend, http://old.wowhead.com/spell=34433
DELETE FROM `spell_proc_event` WHERE `entry` = 28305;
INSERT INTO `spell_proc_event` VALUES (28305, 0, 0, 0, 0, 0, 0, 65536, 0, 0, 0);
-- Move Shadowfiend's Mana Leech Aura from spellscript to creature addon.
UPDATE `creature_template_addon` SET `auras`= '28305' WHERE `entry`=19668;


-- ## Removes Master's Call stun immunity.
INSERT INTO `spell_linked_spell`(`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES 
(54216,-56651,1,'Removes Master''s Call stun immunity');


-- ## Rogue T10 4p bonus
DELETE FROM spell_proc_event WHERE entry = 70803;
INSERT INTO `spell_proc_event` VALUES('70803', '0', '8', '4063232', '8', '0', '0', '0', '0', '0', '0');


-- ## Fix priest bug (kills and honour) spell_priest_spirit_of_redemption.
-- Spell script assignment
DELETE FROM spell_script_names WHERE spell_id = 27827;
INSERT INTO spell_script_names (spell_id, ScriptName) VALUES
(27827, 'spell_priest_spirit_of_redemption');


-- ## Fix Glyph of Succubus.
DELETE FROM `spell_script_names` WHERE `spell_id`=6358;
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES
(6358,'spell_warl_seduction');


-- ## DK: Summon Gargoyle->Gargoyle Strikes
-- http://www.wowhead.com/spell=50514
DELETE FROM spell_bonus_data WHERE entry=51963; 
INSERT INTO spell_bonus_data (entry, direct_bonus, comments) VALUES 
(51963, 1, 'Death Knight - Gargoyle Strike');


-- ## Bladestorm fix.
DELETE FROM `spell_linked_spell` WHERE  `spell_trigger`=51722 AND `spell_effect`=-46924 ;
DELETE FROM `spell_linked_spell` WHERE  `spell_trigger`=676 AND `spell_effect`=-46924;
INSERT INTO `spell_linked_spell` VALUES (676, -46924, 1, '(War)Disarm Cancel Bladestorm');
INSERT INTO `spell_linked_spell` VALUES (51722, -46924, 1, '(Rogue)Dismantle Cancel Bladestorm');
INSERT INTO `spell_linked_spell` VALUES (64058, -46924, 1, '(Priest) Psychic Horror Cancel Bladestorm');
DELETE FROM `spell_linked_spell` WHERE  `spell_trigger`=64346 AND `spell_effect`=-46924;
INSERT INTO `spell_linked_spell` VALUES (64346, -46924, 1, '(Mage)Fiery Payback Cancel Bladestorm');
DELETE FROM `spell_linked_spell` WHERE  `spell_trigger`=53359 AND `spell_effect`=-46924;
INSERT INTO `spell_linked_spell` VALUES (53359, -46924, 1, '(Hunter)Chimera Shot(scorpid) Cancel Bladestorm');


-- ## DK: Ebon Gargoyle
-- http://ru.wowhead.com/npc=27829
delete from `creature_template` where entry = 27829;
INSERT INTO `creature_template` VALUES ('27829', '0', '0', '0', '0', '0', '7534', '7854', '7533', '0', 'Ebon Gargoyle', '', '', '0', '80', '80', '2', '1629', '1629', '0', '1', '1.14286', '1', '0', '722', '986', '0', '742', '1', '2000', '0', '1', '0', '2048', '8', '0', '0', '0', '0', '0', '345', '509', '103', '6', '12288', '0', '0', '0', '0', '0', '0', '0', '0', '0', '51963', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '0', '3', '1', '2', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '8388624', '0', 'npc_pet_dk_ebon_gargoyle', '12340');


-- ## Fix gameobject "train" (Stormwind - Ironforge)
REPLACE INTO `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) VALUES
(5865, 176080, 369, 1, 1, 4.58065, 28.2097, 7.01107, 1.5708, 0, 0, 1, 0, 120, 0, 1),
(5866, 176081, 369, 1, 1, 4.52807, 8.43529, 7.01107, 1.5708, 0, 0, 1, 0, 120, 0, 1),
(5863, 176082, 369, 1, 1, -45.4005, 2492.79, 6.9886, 1.5708, 0, 0, 1, 0, 120, 0, 1),
(16394, 176083, 369, 1, 1, -45.4007, 2512.15, 6.9886, 1.5708, 0, 0, 1, 0, 120, 0, 1),
(5864, 176084, 369, 1, 1, -45.3934, 2472.93, 6.9886, -1.5708, 0, 0, 1, 0, 120, 0, 1),
(15363, 176085, 369, 1, 1, 4.49883, -11.3475, 7.01107, 1.5708, 0, 0, 1, 0, 120, 0, 1);


-- ## Shadow's Edge (Грань Тьмы).
-- http://ru.wowhead.com/quest=24743
replace into `creature_queststarter` (`id`,`quest`) values
(37120, 24743);


-- ## Fix quest The Ring of Blood: The Final Challenge (Nagrand Arena).
DELETE FROM smart_scripts WHERE entryorguid IN (1806900,1806901,18069);
INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES
(18069, 0, 0, 0, 0, 0, 100, 0, 1000, 1000, 3500, 3500, 11, 16033, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 'Mogor - In Combat - Cast Chain Lightning'),
(18069, 0, 1, 0, 0, 0, 100, 0, 4000, 4000, 11000, 13000, 11, 39529, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 'Mogor - In Combat - Cast Flame Shock'),
(18069, 0, 2, 0, 2, 0, 100, 1, 0, 60, 0, 0, 11, 15982, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - At 60% HP - Cast Healing Wave'),
(18069, 0, 3, 0, 2, 0, 100, 1, 0, 30, 0, 0, 11, 28747, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - At 30% HP - Cast Frenzy'),
(18069, 0, 4, 0, 2, 0, 100, 1, 0, 30, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Mogor - At 30% HP - Say Line 0'),
(18069, 0, 5, 0, 6, 0, 100, 1, 0, 0, 0, 0, 80, 1806901, 0, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Death - Run Script'),
(18069, 0, 6, 0, 38, 0, 100, 0, 12, 12, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Data Set - Say Line 1'),
(18069, 0, 7, 0, 38, 0, 100, 0, 13, 13, 0, 0, 1, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Data Set - Say Line 2'),
(18069, 0, 8, 0, 38, 0, 100, 0, 14, 14, 0, 0, 1, 6, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Data Set - Say Line 6'),
(18069, 0, 9, 10, 38, 0, 100, 0, 1, 1, 0, 0, 53, 0, 18069, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Data Set - Start WP'),
(18069, 0, 10, 0, 61, 0, 100, 0, 0, 0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Data Set - Say Line 3'),
(18069, 0, 11, 12, 40, 0, 100, 0, 4, 18069, 0, 0, 54, 100000, 0, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - At WP 4 - Pause Path'),
(18069, 0, 12, 0, 61, 0, 100, 0, 0, 0, 0, 0, 80, 1806900, 0, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - At WP 4 - Run Script'),
(18069, 0, 13, 0, 21, 0, 100, 0, 0, 0, 0, 0, 2, 35, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - Reached Home - Set Faction Friendly'),
(18069, 0, 14, 0, 6, 0, 100, 0, 0, 0, 0, 0, 15, 9977, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Death - Give Quest Credit');

INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES
(1806900, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Script - Say Line 4'),
(1806900, 9, 1, 0, 0, 0, 100, 0, 5000, 5000, 0, 0, 2, 14, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Script - Set Faction Aggressive'),
(1806900, 9, 2, 0, 0, 0, 100, 0, 9, 9, 0, 0, 8, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Script - Set React State Aggressive');

INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES
(1806901, 9, 0, 0, 0, 0, 100, 0, 2000, 2000, 0, 0, 11, 32343, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Script - Cast Revive Self'),
(1806901, 9, 1, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 70, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Script - Revive Self'),
(1806901, 9, 2, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 1, 5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Mogor - On Script - Say Line 5'),
(1806901, 9, 3, 0, 0, 0, 100, 0, 500, 500, 0, 0, 49, 0, 0, 0, 0, 0, 0, 21, 35, 0, 0, 0, 0, 0, 0, 'Mogor - On Script - Attack Closest Player');

UPDATE quest_template SET RequiredNpcOrGo1 = '18069',RequiredNpcOrGoCount1 = '1' WHERE Id =9977;
UPDATE quest_template SET SpecialFlags = '0' WHERE Id =9977;


-- ## Quest: Terokk's Downfall, http://old.wowhead.com/quest=11073
UPDATE `gameobject_template` SET `ScriptName` = 'go_ancient_skull_pile' WHERE `entry` = 185928;
DELETE FROM creature_ai_scripts WHERE creature_id=21838;
UPDATE creature_template SET AIName='', ScriptName='npc_terokk' WHERE entry=21838;
DELETE FROM `creature_text` WHERE `entry`=21838;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(21838,0,0,'Who calls me to this world? The stars are not yet aligned...my powers fail me! You will pay for this!',14,0,0,0,0,0,'Terokk - Summoned & inCombat'),
(21838,1,0,'Show me how tough you are!',12,0,0,0,0,0,'Terokk - Player Chosen'),
(21838,2,0,'Kwa! You cannot kill me, I am immortal!',12,0,0,0,0,0,'Terokk - Immune');
DELETE FROM `creature_template` WHERE (`entry`=97016);
INSERT INTO `creature_template`(`entry`,`difficulty_entry_1`,`difficulty_entry_2`,`difficulty_entry_3`,`KillCredit1`,`KillCredit2`,`modelid1`,`modelid2`,`modelid3`,`modelid4`,`name`,`subname`,`IconName`,`gossip_menu_id`,`minlevel`,`maxlevel`,`exp`,`faction_A`,`faction_H`,`npcflag`,`speed_walk`,`speed_run`,`scale`,`rank`,`mindmg`,`maxdmg`,`dmgschool`,`attackpower`,`dmg_multiplier`,`baseattacktime`,`rangeattacktime`,`unit_class`,`unit_flags`,`unit_flags2`,`dynamicflags`,`family`,`trainer_type`,`trainer_spell`,`trainer_class`,`trainer_race`,`minrangedmg`,`maxrangedmg`,`rangedattackpower`,`type`,`type_flags`,`lootid`,`pickpocketloot`,`skinloot`,`resistance1`,`resistance2`,`resistance3`,`resistance4`,`resistance5`,`resistance6`,`spell1`,`spell2`,`spell3`,`spell4`,`spell5`,`spell6`,`spell7`,`spell8`,`PetSpellDataId`,`VehicleId`,`mingold`,`maxgold`,`AIName`,`MovementType`,`InhabitType`,`HoverHeight`,`Health_mod`,`Mana_mod`,`Armor_mod`,`RacialLeader`,`questItem1`,`questItem2`,`questItem3`,`questItem4`,`questItem5`,`questItem6`,`movementId`,`RegenHealth`,`mechanic_immune_mask`,`flags_extra`,`ScriptName`,`WDBVerified`) VALUES 
('97016','0','0','0','0','0','11686','11686','11686','11686','Terokk Visual Marker Trigger',NULL,NULL,'0','80','80','0','114','114','0','1','1.14286','1','0','104','138','0','252','1','2000','0','1','33555200','0','8','0','0','0','0','0','72','106','26','10','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','','0','7','1','1.35','1','1','0','0','0','0','0','0','0','0','1','0','2','','1');


-- ## DB/Quest: Death from Above, http://ru.wowhead.com/quest=11889
UPDATE creature_template Set `InhabitType` = 4 WHERE entry = 25582;
UPDATE item_template SET `ScriptName` = "item_reinforced_net" WHERE entry = 35278;


-- ## Goldshire
-- Quest: Report to Goldshire, http://ru.wowhead.com/quest=54
DELETE FROM `quest_start_scripts` WHERE `id` = 54;
DELETE FROM `db_script_string` WHERE `entry`=2000000059;
UPDATE `quest_template` SET `StartScript`='54', `SpecialFlags`='0' WHERE `Id`='54';
INSERT INTO `quest_start_scripts` (`id`, `delay`, `command`, `datalong`, `datalong2`, `dataint`, `x`, `y`, `z`, `o`) VALUES
('54','1','15','6245','0','0','0','0','0','0'),
('54','2','1','113','0','0','0','0','0','0'),
('54','2','0','0','0','2000000059','0','0','0','0');
INSERT INTO `db_script_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`) VALUES
('2000000059','You are Dismissed, $N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Ты уволен, $N');


-- ## Elwynn Forest
-- DB/Quest: Garments of the Light, http://ru.wowhead.com/quest=5624
UPDATE `creature_template` SET `faction_A`='11',`faction_H`='11' WHERE `entry`='12423';


-- ## DB/Quest: Shadow Vault Decree, http://ru.wowhead.com/quest=12943
UPDATE item_template SET scriptname = 'shadow_vault_decree' WHERE entry = 41776;


-- ## DB/Quest: Master the Storm, http://www.wowhead.com/quest=11895
INSERT INTO `npc_spellclick_spells` (`npc_entry`, `spell_id`, `cast_flags`, `user_type`) VALUES('26048','63238','0','0');
UPDATE smart_scripts SET id = '0', link = '1' WHERE entryorguid = '26048' AND id = '1'; 
UPDATE smart_scripts SET id = '1' WHERE entryorguid = '26048' AND id = '2'; 
-- Fix Titanium Lockbox Drop
UPDATE `creature_loot_template` SET `ChanceOrQuestChance` = '0.03' WHERE `entry` = '31787' AND `item` = '43624';

-- ## DB/Quest: The Emissary, http://ru.wowhead.com/quest=11626
REPLACE INTO `smart_scripts` VALUES (26452, 0, 1515, 0, 20, 0, 100, 0, 0, 0, 0, 0, 33, 26452, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, "Smart Script");


-- ## DB/Quest: The Thirsty Goblin, http://ru.wowhead.com/quest=2605
DELETE FROM `creature_loot_template` WHERE `item`  = 8428;
INSERT INTO `creature_loot_template` VALUES ('5481', '8428', '80', '1', '0', '1', '1');


-- ## [Trial of Crusader]
-- ## Anub'arak, fix of incorrect YTDB flag.
UPDATE `creature_template` SET  `unit_flags` = 32832 WHERE `entry`= 34564;
UPDATE `creature_template` SET  `unit_flags` = 32832 WHERE `entry`= 34566;
UPDATE `creature_template` SET  `unit_flags` = 32832 WHERE `entry`= 35615;
UPDATE `creature_template` SET  `unit_flags` = 32832 WHERE `entry`= 35616;


-- ## [Blackrock Deepth]
-- ## Door Fix
UPDATE `gameobject_template` SET `faction`=1375 WHERE entry=170577;


-- ## [Uldaman] 
-- ## door fix
UPDATE `gameobject_template` SET `flags` = 18 WHERE `entry` = 124372;
UPDATE `gameobject` SET `id` = 124368 WHERE `guid` = 20913;


-- ## [RS]
-- ## Halion immunities
UPDATE creature_template SET mechanic_immune_mask = mechanic_immune_mask | 650854271 WHERE entry IN (
39863, 40142, -- 10 nm
39864, 40143, -- 25 nm
39944, 40144, -- 10 hc
39945, 40145  -- 25 hc
);
-- Halion damage multipliers
UPDATE creature_template SET dmg_multiplier = 70 WHERE entry IN (39863, 40142);  -- 10 nm
UPDATE creature_template SET dmg_multiplier = 100 WHERE entry IN (39864, 40143); -- 25 nm
UPDATE creature_template SET dmg_multiplier = 100 WHERE entry IN (39944, 40144); -- 10 hc
UPDATE creature_template SET dmg_multiplier = 170 WHERE entry IN (39945, 40145); -- 25 hc


-- ## [Ulduar] 
-- ## http://ru.wowhead.com/npc=33236
DELETE FROM `creature_template` where entry = 33236;
INSERT INTO `creature_template` VALUES ('33236', '34113', '0', '0', '0', '0', '28580', '28581', '0', '0', 'Steelforged Defender', '', '', '0', '80', '80', '2', '1965', '1965', '0', '1', '0.857143', '1', '0', '417', '582', '0', '608', '1', '2000', '2000', '32768', '2048', '0', '0', '0', '0', '0', '341', '506', '80', '7', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'SmartAI', '0', '3', '1', '20', '20', '1', '1', '0', '0', '0', '0', '0', '0', '0', '121', '1', '0', '0', '', '15595');


########
-- ICC #
########

-- ## ICC fixs...
-- update immunity
UPDATE `creature_template` set `mechanic_immune_mask` = 617299803 where entry IN ('36612', '37957', '37958', '37959'); -- Lod Mark'gar
UPDATE `creature_template` set `mechanic_immune_mask` = 617299803 where entry IN ('36855', '38106', '38296', '38297'); -- Lady Deathwhisper
UPDATE `creature_template` set `mechanic_immune_mask` = 617299803 where entry IN ('37813', '38402', '38582', '38583'); -- Deathbringer Saurfang
UPDATE `creature_template` set `mechanic_immune_mask` = 617299803 where entry IN ('36627', '38390', '38549', '38550'); -- Rotface
UPDATE `creature_template` set `mechanic_immune_mask` = 617299803 where entry IN ('36626', '37504', '37505', '37506'); -- Festergut
UPDATE `creature_template` set `mechanic_immune_mask` = 617299803 where entry IN ('36678', '38431', '38585', '38586'); -- Professor Putricide
UPDATE `creature_template` set `mechanic_immune_mask` = 617299803 where entry IN ('37562', '38602', '38760', '38761'); -- Gas Cloud
UPDATE `creature_template` set `mechanic_immune_mask` = 617299803 where entry IN ('37697', '38604', '38758', '38759'); -- Volatile Ooze
UPDATE `creature_template` SET `mechanic_immune_mask` = 617299803 where entry IN ('36980', '38320', '38321', '38322'); -- Ice Tombs / Sindragosa

-- Rotface:
-- remove conditions (target selection changed from TARGET_UNIT_NEARBY_ENTRY to TARGET_UNIT_TARGET_ANY so doesn't need it anymore)
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry`=69508;
/* backup data
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13, 0, 69508, 0, 0, 18, 0, 1, 37986, 0, 0, 0, '', 'Rotface - Slime Spray');
*/
-- Update ICC: Rotface
-- Fix targetting for Ooze Flood ability in encounter Modermiene / Rotface in instance / Instanz ICC / Eiskronenzitadelle / Icecrown Citadel
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceGroup` = 3 AND `SourceEntry` IN (69783, 69797, 69799, 69802) AND `ConditionTypeOrReference` = 33;
INSERT INTO `conditions` (SourceTypeOrReferenceId, SourceGroup, SourceEntry, SourceId, ElseGroup, ConditionTypeOrReference, ConditionTarget, ConditionValue1, ConditionValue2, ConditionValue3, NegativeCondition, ErrorTextId, ScriptName, Comment) VALUES
(13, 3, 69783, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self'),
(13, 3, 69797, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self'),
(13, 3, 69799, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self'),
(13, 3, 69802, 0, 0, 33, 1, 0, 0, 0, 1, 0, '', 'Rotface - Ooze Flood, not self');
-- Add immunities to Little Ooze / Big Ooze in encounter Modermiene / Rotface in instance / Instanz ICC / Eiskronenzitadelle / Icecrown Citadel
UPDATE `creature_template` SET `mechanic_immune_mask` = 650853247 WHERE `entry` IN (36897, 36899, 38138, 38123);
-- Add interrupt immunity to mini bosses (Rimefang / Raufang / Spinestalker / Wirbelpirscher) in encounter Sindragosa in instance / Instanz ICC / Eiskronenzitadelle / Icecrown Citadel
UPDATE `creature_template` SET `mechanic_immune_mask` = 650853247 WHERE `entry` IN (37533, 38220, 37534, 38219);
-- DB/NPCs: Little Ooze (Rotface in ICC) can not be taunted, 2012_09_10_06_world_creature_template.sql
UPDATE `creature_template` SET `flags_extra`=`flags_extra`|256 WHERE `entry` IN (36897, 38138); -- Little Ooze
-- Set speed values to database for Valkyr Shadowguard in encounter The Lich King in instance / Instanz ICC / Eiskronenzitadelle / Icecrown Citadel
UPDATE `creature_template` SET `speed_walk` = 0.642857, `speed_run` = 0.642857, `InhabitType` = 4 WHERE `entry` IN (36609, 39120, 39121, 39122);
-- Set speed values to database for Vile Spirits in encounter The Lich King in instance / Instanz ICC / Eiskronenzitadelle / Icecrown Citadel
UPDATE `creature_template` SET `speed_walk` = 0.5, `speed_run` = 0.5, `InhabitType` = 4 WHERE `entry` IN (37799, 39284, 39285, 39286);

-- Add script
DELETE FROM `spell_script_names` WHERE `ScriptName`='spell_rotface_slime_spray';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(69507, 'spell_rotface_slime_spray'),
(71213, 'spell_rotface_slime_spray'),
(73189, 'spell_rotface_slime_spray'),
(73190, 'spell_rotface_slime_spray');

-- Sindragosa:
-- Fix spell 69762 Unchained Magic 
-- Add internal cooldown with 1 seconds, so multi-spell spells will only apply one stack of triggered spell 69766 (i.e. Chain Heal)
DELETE FROM `spell_proc_event` WHERE `entry` = 69762;
INSERT INTO `spell_proc_event` (entry, SchoolMask, SpellFamilyName, SpellFamilyMask0, SpellFamilyMask1, SpellFamilyMask2, procFlags, procEx, ppmRate, CustomChance, Cooldown) VALUES
(69762, 0, 0, 0, 0, 0, 81920, 0, 0, 0, 1);

-- ## Saurfang:
UPDATE `creature_template` SET `npcflag` = 1 WHERE `entry` = 37187;


-- ## Fix MMaps
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (37955,38434,38435,38436); -- Blood-Queen Lana'thel (ICC)
-- UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (37813,38402,38582,38583); -- Deathbringer Saurfang (ICC)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (36853,38265,38266,38267); -- Sindragosa (ICC)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (37533,38220); -- Rimefang (ICC)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (37534,38219); -- Spinestalker (ICC)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (39747,39823); -- Saviana Ragefire (RS)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (10184,36538); -- Onyxia (Ony)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (24068,31655); -- Annhylde the Caller (UK)
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (11583); -- Nefarian
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (27829); -- Ebon Gargyole
UPDATE creature_template SET InhabitType = InhabitType | 4 WHERE `entry` IN (33293); -- XT002 (Ulduar)

