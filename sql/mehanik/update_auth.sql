-- Mehanik 3 Core, update auth db.


-- ## RBAC
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES
-- players
(195, 24), -- Two side faction characters on the same account
(195, 25), -- Allow say chat between factions
(195, 26), -- Allow channel chat between factions
(195, 27), -- Two side mail interaction
(195, 28), -- See two side who list
(195, 29), -- Add friends of other faction 
(195, 31), -- Use params with .unstuck command
(195, 414), -- in un all
(195, 513), -- maxskill
-- (195, 40), -- Allows to add a gm to friend list 
-- event master
(193, 478), -- group summon (2)
(193, 373), -- gm fly (2)
(193, 497), -- cooldown (2)
(193, 523), -- revive (2)
(193, 488), -- additem (2)
-- game master
(192, 478), -- group summon (3) 
(195, 30); -- Save character without delay with .save command
;


-- ## XD? HACK.
DROP TABLE IF EXISTS `quest_completer`;
CREATE TABLE `quest_completer` (
`id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
PRIMARY KEY (`id`)
)