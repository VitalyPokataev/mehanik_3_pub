----------------------------------------
-- Trinity - Myth Converter --
----------------------------------------

ALTER TABLE `account` DROP COLUMN `mutereason`;
ALTER TABLE `account` DROP COLUMN `muteby`;
-- DROP TABLE `autobroadcast`;
-- DROP TABLE `ip2nation`;
-- DROP TABLE `ip2nationCountries`;
ALTER TABLE `logs` DROP COLUMN `level`;
-- Dump :3
-- DROP TABLE `rbac_account_groups`;
-- DROP TABLE `rbac_account_permissions`;
-- DROP TABLE `rbac_account_roles`;
-- DROP TABLE `rbac_groups`;
-- DROP TABLE `rbac_permissions`;
-- DROP TABLE `rbac_role_permissions`;
-- DROP TABLE `rbac_roles`;
-- DROP TABLE `rbac_security_level_groups`;
ALTER TABLE `realmlist` DROP COLUMN `localAddress`;
ALTER TABLE `realmlist` DROP COLUMN `localSubnetMask`;
ALTER TABLE `realmlist` DROP COLUMN `flag`; -- =>
ALTER TABLE `realmlist` ADD COLUMN `color` tinyint(3) unsigned NOT NULL default '2'; -- <=

-- DROP TABLE `banned_addons`;
-- DROP TABLE `calendar_events`;
-- DROP TABLE `calendar_invites`;
ALTER TABLE `character_equipmentsets` DROP COLUMN `ignore_mask`;
ALTER TABLE `character_queststatus` DROP COLUMN `playercount`;
-- DROP TABLE `character_queststatus_monthly`;
ALTER TABLE `character_queststatus_rewarded` DROP COLUMN `active`;
-- DROP TABLE `character_queststatus_seasonal`;
ALTER TABLE `characters` DROP COLUMN `grantableLevels`;
ALTER TABLE `creature_respawn` DROP COLUMN `mapId`;
ALTER TABLE `gameobject_respawn` DROP COLUMN `mapId`;
ALTER TABLE `guild_member` ADD COLUMN `BankResetTimeMoney` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankRemMoney` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankResetTimeTab0` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankRemSlotsTab0` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankResetTimeTab1` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankRemSlotsTab1` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankResetTimeTab2` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankRemSlotsTab2` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankResetTimeTab3` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankRemSlotsTab3` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankResetTimeTab4` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankRemSlotsTab4` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankResetTimeTab5` int(10) unsigned NOT NULL DEFAULT 0;
ALTER TABLE `guild_member` ADD COLUMN `BankRemSlotsTab5` int(10) unsigned NOT NULL DEFAULT 0;
-- DROP TABLE `guild_member_withdraw`;
-- DROP TABLE `item_loot_items`;
-- DROP TABLE `item_loot_money`;
ALTER TABLE `gm_tickets` DROP COLUMN `haveTicket`, DROP COLUMN `response`;
ALTER TABLE `lag_reports` DROP COLUMN `createTime`, DROP COLUMN `latency`;
-- DROP TABLE `warden_action`;
-- DROP TABLE `lfg_data`;
-- �������� � ����������.
UPDATE characters SET drunk = (drunk * 256) & 0xFF;
ALTER TABLE characters CHANGE drunk drunk smallint(5) unsigned NOT NULL DEFAULT '0';
