/*
 * Copyright (C) 2014 Mehanik 3 <https://bitbucket.org/Vitasic/mehanik_3>
 * Copyright (C) 2013 MythCore 12.52 <https://bitbucket.org/Vitasic/mythcore>
 * Copyright (C) 2013 BattleCore <https://github.com/Vitasic/Battle>
 * Copyright (C) 2013 Vitasic <https://github.com/Vitasic>
 * Copyright (C) 2013 Vitasic <http://vk.com/vit_pokataev>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptPCH.h"
#include "Chat.h"
#include "../../shared/Configuration/Config.h"

#define FROST_EMBLEM 49426
#define TRIUMPH_EMBLEM 47241

class custom_mehanik : public PlayerScript
{
public:
	custom_mehanik() : PlayerScript("custom_mehanik") {}
	void OnLogin(Player *player){
		bool EnableAnnounceOnPlayerStart = sConfigMgr->GetBoolDefault("Enable.Announce.On.Player.Start", true);
		bool EnableMountOnStart = sConfigMgr->GetBoolDefault("Enable.Mount.On.Start", true);
		bool EnableFastMountOnStart = sConfigMgr->GetBoolDefault("Enable.Fast.Mount.On.Start", true);
		bool EnableFlyOnOneLevel = sConfigMgr->GetBoolDefault("Enable.Fly.On.One.Level", true);
		bool EnableCastSpellOnLogin = sConfigMgr->GetBoolDefault("Enable.Cast.Spell.On.Login", true);
		bool EnableCastSpellOnLoginInCity = sConfigMgr->GetBoolDefault("Enable.Cast.Spell.On.Login.In.City", true);
		bool EnableCastSpellOnEvents = sConfigMgr->GetBoolDefault("Enable.Cast.Spell.On.Events", true);

		// AnnounceOnPlayerStart 
		if (EnableAnnounceOnPlayerStart && player->getLevel() == 1){
			std::string msg;
                        msg += " |cffff0000[Admin]:|r";
          		msg += "|cffFF8C00 Player|r |cffD63931";
          		msg += player->GetSession()->GetPlayerName();
          		msg += "|r |cffFF8C00joined us, and will now play on the server :3|r \n";
			sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str());
		} // AnnounceOnPlayerStart

		// mount on start!
		if ((EnableMountOnStart && player->getLevel() == 1) || (EnableFastMountOnStart && player->getLevel() == 1))
        		player->learnSpell(16056,0); // mount

		if (EnableMountOnStart && player->getLevel() == 1)
        		player->learnSpell(33388,0); // 1L

		if (EnableFastMountOnStart && player->getLevel() == 1){
        		player->learnSpell(33388,0); // 1L
        		player->learnSpell(33391,0); // 2L
		} // mount on start

		// fly on level 1 :3
		if (EnableFlyOnOneLevel && player->getLevel() == 1)
			player->CastSpell(player, 70766, true); // http://ru.wowhead.com/spell=70766

		// CastSpellOnLogin
		if(EnableCastSpellOnLogin)
			player->CastSpell(player, 8358, true); // http://ru.wowhead.com/spell=8358

		if(EnableCastSpellOnLoginInCity){
			switch(player->GetTeam()){
				case ALLIANCE:
					if (player->GetZoneId() == 1519){  // stormwind 
						player->SetHealth(player->GetMaxHealth());
						player->CastSpell(player, 1126, true); // http://ru.wowhead.com/spell=1126
					}
					break;
				case HORDE:
					if (player->GetZoneId() == 1637){  // orgrimmar
						player->SetHealth(player->GetMaxHealth());
						player->CastSpell(player, 1126, true); // http://ru.wowhead.com/spell=1126
					}
					break;
			}
			if (player->GetZoneId() == 4395){ // dalaran 
				player->SetHealth(player->GetMaxHealth());
				player->CastSpell(player, 1126, true); // http://ru.wowhead.com/spell=1126
				player->CastSpell(player, 70766, true); // http://ru.wowhead.com/spell=70766 // fly :3
			}
			if (player->GetZoneId() == 3703){ // shatrat
				player->SetHealth(player->GetMaxHealth());
				player->CastSpell(player, 1126, true); // http://ru.wowhead.com/spell=1126
				player->CastSpell(player, 70766, true); // http://ru.wowhead.com/spell=70766 // fly :3
			}
		} // CastSpellOnLoginInCity

		// CastSpellOnEvents
		if(EnableCastSpellOnEvents){
			if(IsEventActive(2)){ // Feast of Winter Veil (15-12-xxxx)
				switch(player->GetTeam()){
					case ALLIANCE:
						switch(player->getGender()){
							case GENDER_MALE: // men
								player->CastSpell(player, 26157, true); // http://ru.wowhead.com/spell=26157
								break;
							case GENDER_FEMALE: // w
								player->CastSpell(player, 26272, true); // http://ru.wowhead.com/spell=26272
								break;
						}
					case HORDE:
						switch(player->getGender()){
							case GENDER_MALE: // men
								player->CastSpell(player, 26273, true); // http://ru.wowhead.com/spell=26273
								break;
							case GENDER_FEMALE: // w
								player->CastSpell(player, 26274, true); // http://ru.wowhead.com/spell=26274
								break;
						}
				}
			} // Feast of Winter Veil (15-12-xxxx)
		} // CastSpellOnEvents

	}

	void OnLevelChanged(Player *player, uint8 newLevel){
		std::string announce;
		bool GiveGold60lvl = sConfigMgr->GetBoolDefault("Give.Gold.60lvl", true);
		bool GiveGold80lvl = sConfigMgr->GetBoolDefault("Give.Gold.80lvl", true);
		bool EnableSnaksOnLevel = sConfigMgr->GetBoolDefault("Enable.Snaks.On.Level", true);

		// Gold + Cast:3
		if (GiveGold60lvl && player->getLevel() == 60){
			player->ModifyMoney(5000000);
			player->CastSpell(player, 8358, true); // http://ru.wowhead.com/spell=8358
			player->CastSpell(player, 1126, true); // http://ru.wowhead.com/spell=1126
			announce += " |cffff0000[Admin]:|r";
          		announce = " |cffFF8C00 Player|r |cffD63931";
          		announce += player->GetSession()->GetPlayerName();
          		announce += "|r |cffFF8C00got 500 gold for level 60!|r \n";
          		sWorld->SendServerMessage(SERVER_MSG_STRING, announce.c_str());
		}
		if (GiveGold80lvl && player->getLevel() == 80){
			player->ModifyMoney(10000000);
			player->CastSpell(player, 8358, true); // http://ru.wowhead.com/spell=8358
			player->CastSpell(player, 1126, true); // http://ru.wowhead.com/spell=1126
			announce += " |cffff0000[Admin]:|r";
          		announce = " |cffFF8C00 Player|r |cffD63931";
          		announce += player->GetSession()->GetPlayerName();
          		announce += "|r |cffFF8C00got 1000 gold for level 80!|r \n";
          		sWorld->SendServerMessage(SERVER_MSG_STRING, announce.c_str());
		} // Gold + Cast

		// Snaks on level
		uint16 count;
		if(EnableSnaksOnLevel){
			switch(urand(1000,2000)%2){
				case 0: count = 1; break;
				case 1: count = 2; break;
			}
			switch(urand(1000,3000)%3){
				case 0: player->AddItem(TRIUMPH_EMBLEM, count); break;
				case 1: player->AddItem(FROST_EMBLEM, count); break;
				case 2: player->ModifyMoney(100000); break; // 10gold
			}
		} // EnableSnaksOnLevel

		bool Action = sConfigMgr->GetBoolDefault("Mehanik.Action", true);
		// �8 �����, 100 ������
		if (player->getLevel() == 80 && Action){ // ���� 80 �������
			player->AddItem(FROST_EMBLEM, 100);
			//player->SetLevel(75);
			switch(player->getClass()){ // �������� �����
			case CLASS_ROGUE:
				player->AddItem(51496, 1);
				break;
			case CLASS_DEATH_KNIGHT:
				player->AddItem(51418, 1);
				break;
			case CLASS_WARRIOR:
				player->AddItem(51545, 1);
				break;
			case CLASS_PRIEST:
				player->AddItem(51486, 1);
				player->AddItem(51491, 1);
				break;
			case CLASS_MAGE:
				player->AddItem(51467, 1);
				break;
			case CLASS_PALADIN:
				player->AddItem(51473, 1);
				player->AddItem(51479, 1);
				break;
			case CLASS_HUNTER:
				player->AddItem(51462, 1);
				break;
			case CLASS_DRUID:
				player->AddItem(51438, 1);
				player->AddItem(51430, 1);
				player->AddItem(51424, 1);
				break;
			case CLASS_SHAMAN:
				player->AddItem(51514, 1);
				player->AddItem(51508, 1);
				player->AddItem(51502, 1);
				break;
			case CLASS_WARLOCK:
				player->AddItem(51540, 1);
				break;
			} // �������� �����
		} // �8 �����, 100 ������
	} // OnLevelChanged

	void OnUpdateZone(Player* player, uint32 newZone, uint32 newArea) {}
};

void AddSC_custom_mehanik()
{
    new custom_mehanik();
}