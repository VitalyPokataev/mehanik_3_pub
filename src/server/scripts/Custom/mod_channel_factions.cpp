/*
 * Copyright (C) 2014 Mehanik 3 <https://bitbucket.org/Vitasic/mehanik_3>
 * Copyright (C) 2013 Vitasic <https://github.com/Vitasic>
 * Copyright (C) 2013 Vitasic <http://vk.com/vit_pokataev>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptPCH.h"
#include "Channel.h"
#include "Player.h"
#include <sstream>

class channel_factions : public PlayerScript
{
    public:
        channel_factions() : PlayerScript("channel_factions") { }

    void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Channel* channel)
    {
        if (!player || !channel)
            return;

        if (!sWorld->getBoolConfig(CONFIG_CHANNEL_FACTIONS))
            return;

        std::stringstream ssMsg;
        ssMsg << ((player->GetTeam() == HORDE) ? "|TInterface\\PVPFrame\\PVP-Currency-Horde:18:18:-3:-3|t" : "|TInterface\\PVPFrame\\PVP-Currency-Alliance:18:18:-3:-3|t") << msg;
        msg = ssMsg.str();
    }
};

void AddSC_channel_factions()
{
    new channel_factions();
}
